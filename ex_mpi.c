#include <netcdf.h>
#include <netcdf_par.h>
#include <mpi.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BAIL(e) do { \
printf("Bailing out in file %s, line %d, error:%s.\n", __FILE__, __LINE__, nc_strerror(e)); \
return e; \
} while (0)

#define FILE "test_par.nc"
#define NDIMS 2
#define DIMSIZEX 11
#define DIMSIZEY 4
#define QTR_DATA (DIMSIZE*DIMSIZE/4)
#define NUM_PROC 4

int
main(int argc, char **argv)
{
    /* MPI stuff. */
    int mpi_namelen;
    char mpi_name[MPI_MAX_PROCESSOR_NAME];
    int mpi_size, mpi_rank;
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    /* Netcdf-4 stuff. */
    int ncid, v1id, dimids[NDIMS];
    size_t start[NDIMS], count[NDIMS];

    int *data, j, i, res;

    /* Initialize MPI. */
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Get_processor_name(mpi_name, &mpi_namelen);
    printf("mpi_name: %s size: %d rank: %d\n", mpi_name,
           mpi_size, mpi_rank);

    /* Create a parallel netcdf-4 file. */
    if ((res = nc_create_par(FILE, NC_NETCDF4, comm,
                             info, &ncid)))
        BAIL(res);

    /* Create two dimensions. */
    if ((res = nc_def_dim(ncid, "d1", DIMSIZEX, dimids)))
       BAIL(res);
    if ((res = nc_def_dim(ncid, "d2", DIMSIZEY, &dimids[1])))
       BAIL(res);

    /* Create one var. */
    if ((res = nc_def_var(ncid, "v1", NC_INT, NDIMS, dimids, &v1id)))
       BAIL(res);

    if ((res = nc_enddef(ncid)))
       BAIL(res);

    /* Set up slab for this process. */
    start[0] = mpi_rank * (DIMSIZEX/mpi_size);
    start[1] = 0;
    if (mpi_rank < mpi_size -1)
        count[0] = DIMSIZEX/mpi_size;
    else
        count[0] = DIMSIZEX/mpi_size + (DIMSIZEX % mpi_size);
    count[1] = DIMSIZEY;
    printf("mpi_rank=%d start[0]=%d start[1]=%d count[0]=%d count[1]=%d\n",
           mpi_rank, start[0], start[1], count[0], count[1]);

    data = (int *) malloc(count[0] * count[1] * sizeof(int));
    for (i = 0; i < count[0]; i++)
	for( j = 0; j < count[1]; j++)
		data[i*count[1] + j] = mpi_rank;

    /*if ((res = nc_var_par_access(ncid, v1id, NC_COLLECTIVE)))
         BAIL(res);*/
    if ((res = nc_var_par_access(ncid, v1id, NC_INDEPENDENT)))
       BAIL(res);

    /* Write slabs of phony data. */
    if ((res = nc_put_vara_int(ncid, v1id, start, count,
                               data)))
       BAIL(res);

    /* Close the netcdf file. */
    if ((res = nc_close(ncid)))
        BAIL(res);

    /* Shut down MPI. */
    MPI_Finalize();

    return 0;
}
