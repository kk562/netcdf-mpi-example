#include <netcdf.h>
#include <netcdf_par.h>
#include <mpi.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#define BAIL(e) do { \
printf("Bailing out in file %s, line %d, error:%s.\n", __FILE__, __LINE__, nc_strerror(e)); \
return e; \
} while (0)

#define FILE "test_par.nc"
#define NDIMS 2
#define DIMSIZEX 11
#define DIMSIZEY 4
#define QTR_DATA (DIMSIZE*DIMSIZE/4)
#define NUM_PROC 4

int
main(int argc, char **argv)
{
    /* MPI stuff. */
    int mpi_namelen;
    char mpi_name[MPI_MAX_PROCESSOR_NAME];
    int mpi_size, mpi_rank;
    MPI_Comm comm = MPI_COMM_WORLD;
    MPI_Info info = MPI_INFO_NULL;

    /* Netcdf-4 stuff. */
    int res, ncid, timeid, varid, varid_int;

    /* Initialize MPI. */
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    MPI_Get_processor_name(mpi_name, &mpi_namelen);
    printf("mpi_name: %s size: %d rank: %d\n", mpi_name,
           mpi_size, mpi_rank);

#ifdef USE_MPI
    /* Create a parallel netcdf-4 file. */
    if ((res = nc_create_par(FILE, NC_NETCDF4, comm,
                            info, &ncid)))
         BAIL(res);
#else
    if ((res = nc_create(FILE, NC_NETCDF4,
                            &ncid)))
         BAIL(res);
#endif

    if (res = nc_def_var(ncid, "int_variable", NC_INT, 0, NULL, &varid_int))
	BAIL(res);

    if (res = nc_def_var(ncid, "formatted", NC_STRING, 0, NULL, &varid))
	BAIL(res);


    nc_var_par_access(ncid, varid, NC_INDEPENDENT);

    const int value_int = 8;
    if (res = nc_put_var_int(ncid, varid_int, &value_int))
	BAIL(res);


    const char *time_string = "2023-03-29";
    if (res = nc_put_var_string(ncid, varid, &time_string))
	BAIL(res);


    if ((res = nc_close(ncid)))
        BAIL(res);

    /* Shut down MPI. */
    MPI_Finalize();

    return 0;
}
